import Vue from 'vue';
import regeneratorRuntime from 'regenerator-runtime'; // for support async functions
import Root from './components/Root/Root.vue';

new Vue({
    render: function (createElement) {
      return createElement(Root)
    },
    el: '#vue-root'
})