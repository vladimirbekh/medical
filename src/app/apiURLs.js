export default {
    USERS: {
        GET: '/api/v1/users/get/',
        CREATE: '/api/v1/users/create/',
    }
}