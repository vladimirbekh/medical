const {ControllerAuth} = require('../Controllers/Auth');
const passport = require('passport');

exports.RouteAuth  = [
    {
        url: '/api/v1/auth',
        method: 'post',
        middleCallback: (req, res, next) => {
            passport.authenticate(
                'local', 
                function(error, user, info) {
                    if(error) {
                        return res.status(500).json(error);
                    }
                    if(!user) {
                        return res.status(401).json(info.message);
                    }
                    res.user = user;
                    next();
                }
            )(req, res, next);
        },
        callback: async (req, res) => {
            res.send({
                user: res.user
            });
        }
    }
];