const {ControllerUser} = require('../Controllers/User');

exports.RouteUser  = [
    {
        url: '/api/v1/users/:action',
        method: 'post',
        middleCallback: (req, res, next) => {
            if (!req.isAuthenticated()) {
                return res.status(401).send(
                    {
                        message: 'need Authorize'
                    }
                );
            }
            next();
        },
        callback: async (req, res) => {
            let result = await ControllerUser.run(req);
            res.send(result);
        }
    }
];