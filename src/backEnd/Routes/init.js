const { RouteAuth } = require('./Auth');
const { RouteUser } = require('./User');
const { routeParams } = require('./routeParams');

const RouteRules = [ ...RouteUser, ...RouteAuth ];

exports.initRoutes = (app) => {
    for(let routeParam of routeParams) {
        app.param(routeParam.name, routeParam.callback);
    }
    
    for(let route of RouteRules) {
        if(route.middleCallback) {
            app[route.method](route.url, route.middleCallback, route.callback);
        } else {
            app[route.method](route.url, route.callback);
        }
    }
}