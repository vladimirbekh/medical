const session = require("express-session");
const secretKey = 'firstMedicalSecret';

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const {ControllerUser} = require('../Controllers/User');

exports.initAuth = (app) => {
    app.use(session(
        { 
            secret: secretKey,
            resave: true,
            saveUninitialized: true
        }
    ));

    passport.use( 
        'local', 
        new LocalStrategy(
            {
                usernameField: 'login',
            }, 
            initLocalStrategy
        )
    );

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });
      
    passport.deserializeUser(async function(id, done) {
        const DBquery = {
            where: {
                'id': id
            }
        }
        const user = await ControllerUser.getUser(DBquery);
        done(null, user);
    });

    app.use(passport.initialize());
    app.use(passport.session());
}

async function initLocalStrategy(username, password, done) {
    const DBquery = {
        where: {
            'login': username
        }
    }
    const result = await ControllerUser.getUser(DBquery);
    if(result.result == 'success') {
        if(result.info.length == 0) {
            return done(null, false, {
                message: 'User not found',
            });
        } else {
            const userInfo = result.info[0];
            const passwordCompareResult = await ControllerUser.checkPassword(password, userInfo.password);

            if(passwordCompareResult) {
                return done(null, userInfo.dataValues);
            } else {
                return done(null, false, {
                    message: 'Wrong password',
                });
            }
        }
    } else {
        console.log(result);
    }
}