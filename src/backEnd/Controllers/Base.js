class ControllerBase {
    async tryDoAction(promise) {
        try {
            const successInfo = await promise;
            return {
                result: 'success',
                info: successInfo,
            }
        } catch(e) {
            return {
                result: 'error',
                error: e,
            }
        }
    }
}

exports.ControllerBase = ControllerBase;