const bcrypt = require('bcrypt');
const saltRounds = 10;

const {ModelUser} = require('../Models/User');
const {ControllerBase} = require('./Base');

class ControllerUser extends ControllerBase {
    async run(request) {
        const methodName = request.action + 'User';
        if( typeof this[methodName] == 'function' ) {
            return await this[methodName](request.body);
        } else {
            return {
                result: 'error',
                error: {
                    message: 'action not found'
                }
            }
        }
    }

    async createUser(body) {
        body.password = await getHashPassword(body.password);
        
        const action = ModelUser.build(body).save();
        return await this.tryDoAction( action );
    }

    async getUser(body) {
        const action = ModelUser.findAll(body);
        return await this.tryDoAction( action );
    }

    async getHashPassword(password) {
        return password ? await bcrypt.hash(password, saltRounds) : null;
    }

    async checkPassword(password, passwordHash) {
        return password && passwordHash ? await bcrypt.compare(password, passwordHash) : false;
    }
}

exports.ControllerUser = new ControllerUser();