const { DataTypes } = require('sequelize');
const { connection } = require('../DB/connection');

exports.ModelUser = connection.define('User', {
  login: {
    type: DataTypes.STRING,
    allowNull: false
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
    tableName: 'Users'
});