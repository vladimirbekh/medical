const {install} = require('./install');
const { connection } = require('./connection');

exports.initDB = async () => {
    try {
        await connection.authenticate();
        await install();
        console.log('Подключение к MySQL установлено');
    } catch (error) {
        return console.error('Unable to connect to the database:', error);
    }
}