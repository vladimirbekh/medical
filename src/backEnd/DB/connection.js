const { Sequelize } = require('sequelize');
const { authSettings } = require('./authInfo');

exports.connection = new Sequelize(
    authSettings.database, 
    authSettings.user, 
    authSettings.password, 
    authSettings.options
);