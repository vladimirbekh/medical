const {ModelUser} = require('../Models/User');

exports.install = async () => {
    return await ModelUser.sync();
}