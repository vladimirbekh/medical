const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('../../webpack.config');
const compiler = webpack(config);

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath,
}));

app.use( webpackHotMiddleware(compiler) );
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.listen(port, () => {
    console.log(`Server started at http://localhost::${port}`);
});


const { initDB } = require('./DB/initDB');
initDB();

const { initAuth } = require('./Auth/init');
initAuth(app);

const { initRoutes } = require('./Routes/init');
initRoutes(app);